import boto3
from botocore.exceptions import ClientError
import psycopg2
import requests
import time



def main(aws_access_key_id, aws_secret_access_key, region_name, db_masterpassword):

  # Returns only Postgres DBInstances existing on client's Account.
  def get_dbinstance():
    try:
      # Creating a session.
      session = boto3.session.Session(aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, region_name=region_name)
      client = session.client('rds')

      # getting the response
      response = client.describe_db_instances()
      # print(response)
        
      # Filtering only postgres DBInstance from All Available DBInstances.
      all_available_dbinstances = []
      for DBinstance in response['DBInstances']:
        if DBinstance['Engine'] == 'postgres':
          data = {}
          data['endpoint'] = DBinstance['Endpoint']['Address']
          data['port'] = DBinstance['Endpoint']['Port']
          data['user'] = DBinstance['MasterUsername']
          data['region'] = DBinstance['AvailabilityZone']
          
          all_available_dbinstances.append(data)
      return all_available_dbinstances
      
    except ClientError as e:
      raise Exception("boto3 client error in retrieves_all_database_details: " + e.__str__())
    except Exception as e:
      raise Exception("Unexpected error in retrieves_all_database_details: " + e.__str__())
  print("\nAll Postgres DBinstances available on the client's AWS Account: ", get_dbinstance())


  # Get the list of DBInstances.
  dbinstances = get_dbinstance()


  # Executes only if there is any DBinstance 
  for dbinstance in dbinstances:
    # Only for testing purpose
    print(f"\nDBInstance {dbinstances.index(dbinstance) + 1} Data: {dbinstance}\n")

    # Returns all databases existing in a Postgres DBInstance.
    def get_databases():
      try:
        # Connecting to Postgres server and creating an Operator.
        conn = psycopg2.connect(host=dbinstance['endpoint'], port=dbinstance['port'], database="postgres", user=dbinstance['user'], password=db_masterpassword)
        cur = conn.cursor()
        
        cur.execute("""select datname from pg_catalog.pg_database where datistemplate!='true' and datname!='postgres' and datname!='rdsadmin';""")
        query_result = cur.fetchall()
        # print(query_result)

        raw_databases_list = []
        for record in query_result:
            raw_databases_list.append(record)

        filtered_databases_list = []
        for tup in raw_databases_list:
          filtered_databases_list.append(tup[0])

        return filtered_databases_list

      except Exception as e:
          print("Database connection failed due to {}".format(e)) 
      finally:
          conn.close()
    print("Databases: ", get_databases()) # Test get_databases.



    # Updating the list with all available databases in a DBInstance.
    dbinstance['databases'] = get_databases()
    # print("DBInstance Data after Updating:", dbinstance) # To test whether dbinstance dictionary has updated or not. 



    # Returns all tables existing in a particular postgres database
    def get_tables(db_name):
      try:
        conn = psycopg2.connect(host=dbinstance['endpoint'], port=dbinstance['port'], database=db_name, user=dbinstance['user'], password=db_masterpassword)
        cur = conn.cursor()
        cur.execute("""select tablename from pg_catalog.pg_tables where schemaname != 'pg_catalog' and schemaname != 'information_schema';""")

        query_result = cur.fetchall()
        # print(query_result)
        tables = []
        for record in query_result:
            tables.append(record[0])
        return tables

      except Exception as e:
          print("Database connection failed due to {}".format(e)) 
      finally:
          conn.close()

    # Testing get_tables with all databases
    for db in dbinstance['databases']:
      print(f"Tables in {db}: ", get_tables(db))
    print()

    # Classification Engine.
    scan_api_endpoint = "https://dapis.microsec.ai/api/dlp-api/classification/scan"


    # Submiting data to Classification Engine
    def data_submission():
      for database in dbinstance['databases']:
        try:
          conn = psycopg2.connect(host=dbinstance['endpoint'], port=dbinstance['port'], database=database, user=dbinstance['user'], password=db_masterpassword)
          cur = conn.cursor()
          for table in get_tables(database):

            offset = 0
            cur.execute(f"""SELECT count(*) FROM {table}""")
            total_rows = cur.fetchone()[0]
            print(f"Total Rows in {database} / {table}: {total_rows}")
            
            while total_rows > 0:
              if total_rows <= 1000:
                query = f"""SELECT * FROM {table} offset {offset}"""
                total_rows = 0
              else:
                query = f"""SELECT * FROM {table} limit 1000 offset {offset}"""
                offset += 1000
                total_rows -= 1000

              cur.execute(query)
              query_result = cur.fetchall()

              query_result_string = str(query_result)
              data = {
                  "profile_id": "5402476c-df7d-44bc-9c1b-b0a6afb931d7",
                  "company_id": "5402476c-df7d-44bc-9c1b-b0a6afb931d7",
                  "policy_name": "deepolicy",
                  "text": query_result_string
              }
              print("submiting data to Classification Engine...")
              r = requests.post(url=scan_api_endpoint, json=data)
              
              print(f"{query_result} Length: {len(query_result)}\nstatus: {r.json()['status']}; scan_id: {r.json()['result']['scan_id']}")
              print("Data Submitted..!!")
              print(f"\nRows to be Read: {total_rows}, Offset Value: {offset}")

            print(f"Successfully submitted the data from {database} / {table} to Classification Engine.\n")

        except Exception as e:
          print(e)
        finally:
          conn.close()
    start = time.strftime("%H:%M:%S")
    data_submission()
    end = time.strftime("%H:%M:%S")
    print("Start: {}, End: {}".format(start, end))



# IAM Credentials.
AWS_ACCESS_KEY_ID = "AKIA5CRRDDIAACXFUJNU"
AWS_SECRET_ACCESS_KEY = "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"
REGION = "ap-south-1"
DB_PASSWORD = "postgres"


main(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, REGION, DB_PASSWORD)